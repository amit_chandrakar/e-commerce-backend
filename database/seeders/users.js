import { faker } from '@faker-js/faker';
import bcrypt from 'bcrypt';

export function createRandomUser () {
    return {
        name: faker.name.fullName(),
        email: faker.internet.email(),
        password: bcrypt.hashSync(faker.internet.password(), 10),
        mobile: faker.phone.number(),
        image: faker.image.avatar(),
        role: 'admin',
        status: 'active',
        verifiedAt: new Date(),
        createdAt: new Date()
    };
}

// export const USERS = faker.helpers.multiple(createRandomUser, {
//     count: 5
// });
