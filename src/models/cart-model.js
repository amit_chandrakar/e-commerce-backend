import mongoose from 'mongoose';
import cartSchema from '../schema/cart-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('Cart', cartSchema); // (<CollectionName>, <CollectionSchema>)
