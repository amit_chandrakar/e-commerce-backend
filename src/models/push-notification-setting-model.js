import mongoose from 'mongoose';
import pushNotificationSettingSchema from '../schema/push-notification-setting-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('PushNotificationSettings', pushNotificationSettingSchema); // (<CollectionName>, <CollectionSchema>)
