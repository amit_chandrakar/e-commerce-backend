import mongoose from 'mongoose';
import smsSettingSchema from '../schema/sms-setting-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('SMSSetting', smsSettingSchema); // (<CollectionName>, <CollectionSchema>)
