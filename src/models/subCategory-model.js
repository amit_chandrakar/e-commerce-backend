import mongoose from 'mongoose';
import subCategorySchema from '../schema/subCategory-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('SubCategory', subCategorySchema); // (<CollectionName>, <CollectionSchema>)
