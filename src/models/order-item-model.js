import mongoose from 'mongoose';
import orderItemSchema from '../schema/order-item-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('OrderItem', orderItemSchema); // (<CollectionName>, <CollectionSchema>)
