import mongoose from 'mongoose';
import socialAuthSettingSchema from '../schema/social-auth-setting-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('SocialAuthSettings', socialAuthSettingSchema); // (<CollectionName>, <CollectionSchema>)
