import mongoose from 'mongoose';
import subscriptionSchema from '../schema/subscription-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('Subscription', subscriptionSchema); // (<CollectionName>, <CollectionSchema>)
