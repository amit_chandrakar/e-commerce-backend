import mongoose from 'mongoose';
import categorySchema from '../schema/category-schema.js';
mongoose.set('strictQuery', false);

export default mongoose.model('Category', categorySchema); // (<CollectionName>, <CollectionSchema>)
