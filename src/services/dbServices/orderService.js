import HttpError from '../../services/httpErrorService.js';
import mongoose from 'mongoose';
import Order from '../../models/order-model.js';
import Cart from '../../models/cart-model.js';

const DEFAULTS = {
    LIMIT: 10,
    SORT: 'name',
    STATUS: 'All',
    NAME: 'All'
};

/**
 * Retrieves orders based on the provided request parameters.
 * @param {Object} req - The request object.
 * @returns {Promise<Object>} - A promise that resolves to an object containing the retrieved orders.
 * @throws {HttpError} - If an error occurs while retrieving the orders.
 */
const get = async req => {
    let orders;

    try {
        const page = parseInt(req.query.page, 10) - 1 || 0;
        const limit = parseInt(req.query.limit, 10) || DEFAULTS.LIMIT;
        const sort = req.query.sort || DEFAULTS.SORT;
        // const search = req.query.search || '';
        // const status = req.query.status || DEFAULTS.STATUS;

        const getBaseAggregation = () => {
            // return Order.find({ status: { $regex: search, $options: 'i' } })
            //     .where(status !== DEFAULTS.STATUS ? { status } : {});

            return Order.aggregate([
                {
                    $lookup: {
                        from: 'users',
                        localField: 'userId',
                        foreignField: '_id',
                        as: 'user'
                    }
                },
                {
                    $unwind: '$user' // Unwind the 'category' array
                },
                {
                    $lookup: {
                        from: 'shippingAddresses',
                        localField: 'addressId',
                        foreignField: '_id',
                        as: 'address'
                    }
                },
                {
                    $unwind: '$address' // Unwind the 'subcategory' array
                }
            ]);
        };

        orders = await getBaseAggregation()
            // .skip(page * limit)
            // .limit(limit)
            .sort({ [sort]: 1 })
            .exec();

        const total = await Order.countDocuments();

        return {
            status: 'success',
            total,
            page: page + 1,
            limit,
            orders
        };
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Finds a order by its ID.
 * @param {string} orderId - The ID of the order to find.
 * @returns {Promise<Object>} - A promise that resolves to the found order object.
 * @throws {HttpError} - If the order ID is invalid or the order is not found.
 */
const find = async orderId => {
    try {
        if (!mongoose.Types.ObjectId.isValid(orderId)) {
            throw new HttpError('Invalid Order ID format', 400);
        }

        orderId = mongoose.Types.ObjectId(orderId);
        // const order = await Order.findById(orderId);

        let order = await Order.aggregate([
            {
                $match: { _id: orderId }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'userId',
                    foreignField: '_id',
                    as: 'user'
                }
            },
            {
                $unwind: '$user'
            },
            {
                $lookup: {
                    from: 'shippingAddresses',
                    localField: 'addressId',
                    foreignField: '_id',
                    as: 'address'
                }
            },
            {
                $unwind: '$address'
            },
            {
                $lookup: {
                    from: 'orderItems',
                    localField: '_id',
                    foreignField: 'orderId',
                    as: 'orderItems'
                }
            },
            {
                $unwind: '$orderItems'
            },
            {
                $lookup: {
                    from: 'products',
                    localField: 'orderItems.productId',
                    foreignField: '_id',
                    as: 'orderItems.product'
                }
            },
            {
                $group: {
                    _id: '$_id',
                    user: { $first: '$user' },
                    address: { $first: '$address' },
                    orderItems: { $push: '$orderItems' },
                    // Include other fields from the order document
                    // Example: status, createdAt, etc.
                    status: { $first: '$status' },
                    createdAt: { $first: '$createdAt' },
                    deliveryInstruction: { $first: '$deliveryInstruction' }
                }
            },
            {
                $limit: 1
            }
        ])
            .exec();

        order = order[0];

        if (!order) {
            throw new HttpError('Order not found', 404);
        }

        return order;
    } catch (err) {
        // If the error is already an HttpError, throw it as-is
        if (err instanceof HttpError) {
            throw err;
        }
        // Otherwise, wrap it in a generic HttpError
        throw new HttpError(err.message, 500);
    }
};

/**
 * Updates a order with the given data.
 * @param {string} orderId - The ID of the order to update.
 * @param {object} data - The data to update the order with.
 * @param {string} data.code - The new code for the order.
 * @param {string} data.type - The new type for the order.
 * @param {number} data.discount - The new discount for the order.
 * @param {Date} data.fromDate - The new start date for the order.
 * @param {Date} data.toDate - The new end date for the order.
 * @param {string} data.status - The new status for the order.
 * @returns {Promise<object>} The updated order.
 */
const update = async (orderId, data) => {
    const { companyId, code, type, discount, discountType, fromDate, toDate, status } = data;

    // eslint-disable-next-line prefer-const
    let order = await find(orderId);
    order.code = code;
    order.type = type;
    order.discount = discount;
    order.fromDate = fromDate;
    order.toDate = toDate;
    order.status = status;
    order.discountType = discountType;
    order.companyId = companyId;
    order.updatedAt = Date.now();

    try {
        await order.save();
    } catch (err) {
        throw new HttpError(err.message, 500);
    }

    return order;
};

/**
 * Deletes a order by setting its deletedAt property to the current date and time.
 * @param {string} orderId - The ID of the order to be deleted.
 * @returns {Promise<boolean>} - A promise that resolves to true if the order is successfully deleted.
 * @throws {HttpError} - If an error occurs while deleting the order.
 */
const destroy = async orderId => {
    try {
        // Soft delete
        const order = await find(orderId);
        order.deletedAt = Date.now();
        await order.save();

        return true;
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Counts the number of orders for a given company.
 * @param {string} companyId - The ID of the company.
 * @returns {Promise<number>} The number of orders.
 * @throws {HttpError} If an error occurs while counting the orders.
 */
const count = async (companyId) => {
    try {
        const order = await Order.countDocuments({ companyId });

        return order;
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Performs bulk actions on orders.
 * @param {Object} data - The data containing the action and order IDs.
 * @param {string} data.action - The action to perform (delete, active, inactive, restore).
 * @param {Array<string>} data.couponIds - The IDs of the orders to perform the action on.
 * @returns {Promise<Object>} - A promise that resolves to the updated order object(s).
 * @throws {HttpError} - If an error occurs during the bulk action.
 */
const bulkAction = async (data) => {
    const { action, couponIds } = data;

    try {
        let order;

        switch (action) {
        case 'delete':
            order = await Order.updateMany(
                { _id: { $in: couponIds } },
                { deletedAt: Date.now() }
            );
            break;
        case 'active':
            order = await Order.updateMany(
                { _id: { $in: couponIds } },
                { status: 'active' }
            );
            break;
        case 'inactive':
            order = await Order.updateMany(
                { _id: { $in: couponIds } },
                { status: 'inactive' }
            );
            break;
        case 'restore':
            order = await Order.updateMany(
                { _id: { $in: couponIds } },
                { deletedAt: null }
            );
            break;
        default:
            throw new HttpError('Invalid action', 400);
        }

        return order;
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Generates a new order ID.
 * @returns {Promise<number>} The generated order ID.
 * @throws {HttpError} If there is an error while generating the order ID.
 */
// const generateOrderId = async () => {
//     try {
//         const order = await Order.find().sort({ createdAt: -1 }).limit(1);
//         let orderId = 0;
//         if (order.length > 0) {
//             orderId = parseInt(order[0].orderNumber) + 1;
//         } else {
//             orderId = 1;
//         }

//         return orderId;
//     } catch (err) {
//         throw new HttpError(err.message, 500);
//     }
// };

/**
 * Place an order.
 *
 * @param {Object} req - The request object.
 * @param {Object} res - The response object.
 * @param {Function} next - The next middleware function.
 * @returns {Object} - The response object containing the status, message, and order data.
 * @throws {HttpError} - If there is an error while saving the order.
 */
const store = async (req, res, next) => {
    const sessionId = req.params.sessionId;

    // find the cart by sessionId
    const cart = await Cart.findOne({ sessionId });

    if (!cart) {
        return next(new HttpError('Cart not found', 404));
    }

    // Now you can use generateOrderId() properly
    // let orderNumber = await generateOrderId();
    // orderNumber = orderNumber.toString().padStart(6, '0');

    // Create order
    const orderData = {
        userId: cart.userId,
        shippingAddressId: cart.shippingAddressId,
        subTotal: cart.subTotal,
        discount: cart.discount,
        total: cart.total,
        tax: cart.tax,
        deliveryFee: cart.deliveryFee,
        deliveryInstruction: cart.deliveryInstruction,
        paymentMethod: cart.paymentMethod,
        status: 'Ordered',
        createdAt: Date.now()
    };

    let order = new Order(orderData);

    try {
        order = await order.save();
    } catch (err) {
        throw new HttpError(err.message, 500);
    }

    // Insert orderId into cart
    cart.orderId = order._id;
    await cart.save();

    return {
        status: 'success',
        message: 'Order placed successfully',
        data: {
            order
        }
    };
};

export default { get, find, update, destroy, bulkAction, count, store };
