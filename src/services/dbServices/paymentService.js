import HttpError from '../../services/httpErrorService.js';
import Payment from '../../models/payment-model.js';

/**
 * Retrieves payments with additional information.
 * @param {Object} req - The request object.
 * @returns {Promise<Object>} - A promise that resolves to an object containing the status, total count, and payments.
 * @throws {HttpError} - If an error occurs while retrieving the payments.
 */
const get = async (req) => {
    let payments;

    try {
        const getBaseAggregation = () => {
            return Payment.aggregate([
                {
                    $lookup: {
                        from: 'orders',
                        localField: 'orderId',
                        foreignField: '_id',
                        as: 'order'
                    }
                },
                {
                    $unwind: '$order'
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'createdBy',
                        foreignField: '_id',
                        as: 'user'
                    }
                },
                {
                    $unwind: '$user'
                }
            ]);
        };

        payments = await getBaseAggregation().exec();

        const total = await Payment.countDocuments();

        return {
            status: 'success',
            total,
            payments
        };
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Generates a unique payment ID.
 * @returns {Promise<number>} The generated payment ID.
 * @throws {HttpError} If an error occurs while generating the payment ID.
 */
const generatePaymentId = async () => {
    try {
        const payment = await Payment.find().sort({ createdAt: -1 }).limit(1);
        let paymentId = 1;
        if (payment.length > 0) {
            paymentId = payment[0].paymentId + 1;
        }
        return paymentId;
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

// Store payment
const store = async (req) => {
    const { orderId, amount, status, paymentId, paymentMethod } = req.body;

    try {
        const payment = new Payment({
            orderId,
            amount,
            status,
            paymentId,
            paymentMethod
        });
        await payment.save();

        return {
            status: 'success',
            message: 'Payment created successfully',
            payment
        };
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

export default { get, generatePaymentId, store };
