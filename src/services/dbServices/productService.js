import HttpError from '../../services/httpErrorService.js';
import mongoose from 'mongoose';
import Product from '../../models/product-model.js';
import { ObjectId } from 'mongodb';

/**
 * Retrieves a list of products based on the provided request parameters.
 * @param {Object} req - The request object.
 * @returns {Promise<Object>} - A promise that resolves to an object containing the retrieved products.
 * @throws {HttpError} - If an error occurs while retrieving the products.
 */
const get = async req => {
    let products;

    try {
        const getBaseAggregation = () => {
            return Product.aggregate([
                {
                    $lookup: {
                        from: 'categories',
                        localField: 'categoryId',
                        foreignField: '_id',
                        as: 'category'
                    }
                },
                {
                    $unwind: '$category' // Unwind the 'category' array
                },
                {
                    $lookup: {
                        from: 'subCategories',
                        localField: 'subCategoryId',
                        foreignField: '_id',
                        as: 'subCategory'
                    }
                },
                {
                    $unwind: {
                        path: '$subCategory',
                        preserveNullAndEmptyArrays: true // Include documents without matching subCategory
                    }
                },
                {
                    $lookup: {
                        from: 'brands',
                        localField: 'brandId',
                        foreignField: '_id',
                        as: 'brand'
                    }
                },
                {
                    $unwind: {
                        path: '$brand',
                        preserveNullAndEmptyArrays: true // Include documents without matching subCategory
                    }
                }
            ]);
        };

        products = await getBaseAggregation().exec();

        const total = await Product.countDocuments();

        return {
            status: 'success',
            total,
            products
        };
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Stores a product in the database.
 *
 * @param {Object} data - The data of the product to be stored.
 * @param {string} data.companyId - The ID of the company.
 * @param {string} data.categoryId - The ID of the category.
 * @param {string} data.subCategoryId - The ID of the sub-category.
 * @param {string} data.brandId - The ID of the brand.
 * @param {string} data.name - The name of the product.
 * @param {string} data.slug - The slug of the product.
 * @param {string} data.shortDescription - The short description of the product.
 * @param {string} data.longDescription - The long description of the product.
 * @param {string} data.featureImage - The URL of the feature image of the product.
 * @param {Array<string>} data.images - The URLs of the images of the product.
 * @param {number} data.price - The price of the product.
 * @param {number} data.discount - The discount amount of the product.
 * @param {string} data.discountType - The type of discount (e.g. 'percentage', 'amount').
 * @param {number} data.totalQuantity - The total quantity of the product.
 * @param {number} data.quantityInStock - The quantity of the product in stock.
 * @param {number} data.displayOrder - The display order of the product.
 * @param {string} data.status - The status of the product.
 * @returns {Promise<Object>} - A promise that resolves to the stored product.
 * @throws {HttpError} - If there is an error while saving the product.
 */
const store = async data => {
    const { companyId, categoryId, subCategoryId, brandId, name, slug, shortDescription, longDescription, featureImage, images, price, discount, discountType, totalQuantity, quantityInStock, displayOrder, status, sku, tags } = data;

    const product = new Product({ companyId, categoryId, subCategoryId, brandId, name, slug, shortDescription, longDescription, featureImage, images, price, discount, discountType, totalQuantity, quantityInStock, displayOrder, status, sku, tags, createdAt: Date.now() });

    try {
        await product.save();
    } catch (err) {
        throw new HttpError(err.message, 500);
    }

    return product;
};

/**
 * Finds a product by its ID.
 * @param {string} productId - The ID of the product.
 * @returns {Promise<Object>} - The found product.
 * @throws {HttpError} - If the product ID is invalid or the product is not found.
 */
const find = async productId => {
    try {
        productId = mongoose.Types.ObjectId(productId);

        if (!mongoose.Types.ObjectId.isValid(productId)) {
            throw new HttpError('Invalid Product ID format', 400);
        }

        const product = await Product.findById(productId);

        if (!product) {
            throw new HttpError('Product not found', 404);
        }

        return product;
    } catch (err) {
        // If the error is already an HttpError, throw it as-is
        if (err instanceof HttpError) {
            throw err;
        }
        // Otherwise, wrap it in a generic HttpError
        throw new HttpError(err.message, 500);
    }
};

/**
 * Updates a product in the database.
 *
 * @param {string} productId - The ID of the product to update.
 * @param {object} data - The updated data for the product.
 * @returns {Promise<object>} - The updated product object.
 * @throws {HttpError} - If there is an error while updating the product.
 */
const update = async (productId, data) => {
    const { categoryId, subCategoryId, brandId, name, slug, shortDescription, longDescription, featureImage, images, price, discount, discountType, totalQuantity, quantityInStock, displayOrder, status, sku, tags } = data;

    // eslint-disable-next-line prefer-const
    let product = await find(productId);
    product.categoryId = categoryId || product.categoryId;
    product.subCategoryId = subCategoryId || product.subCategoryId;
    product.brandId = brandId || product.brandId;
    product.name = name || product.name;
    product.slug = slug || product.slug;
    product.shortDescription = shortDescription || product.shortDescription;
    product.longDescription = longDescription || product.longDescription;
    product.featureImage = featureImage || product.featureImage;
    product.images = images || product.images;
    product.price = price || product.price;
    product.discount = discount || product.discount;
    product.discountType = discountType || product.discountType;
    product.totalQuantity = totalQuantity || product.totalQuantity;
    product.quantityInStock = quantityInStock || product.quantityInStock;
    product.displayOrder = displayOrder || product.displayOrder;
    product.status = status || product.status;
    product.sku = sku || product.sku;
    product.tags = tags || product.tags;
    product.updatedAt = Date.now();

    try {
        await product.save();
    } catch (err) {
        throw new HttpError(err.message, 500);
    }

    return product;
};

/**
 * Soft deletes a product by setting the deletedAt property to the current date and time.
 * @param {string} productId - The ID of the product to be deleted.
 * @returns {Promise<boolean>} - A promise that resolves to true if the product is successfully deleted.
 * @throws {HttpError} - If an error occurs during the deletion process.
 */
const destroy = async (productId) => {
    productId = new ObjectId(productId);

    // Soft delete
    try {
        const data = await Product.findOne({ _id: productId });
        data.deletedAt = Date.now();
        await data.save();

        return true;
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Counts the number of products for a given company.
 *
 * @param {string} companyId - The ID of the company.
 * @returns {Promise<number>} - The number of products.
 * @throws {HttpError} - If an error occurs while counting the products.
 */
const count = async (companyId) => {
    try {
        const product = await Product.countDocuments({ companyId });

        return product;
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Performs bulk actions on products.
 * @param {Object} data - The data containing the action and product IDs.
 * @param {string} data.action - The action to perform (delete, active, inactive, restore).
 * @param {Array<string>} data.productIds - The IDs of the products to perform the action on.
 * @returns {Promise<Object>} - A promise that resolves to the updated product(s).
 * @throws {HttpError} - If an error occurs during the bulk action.
 */
const bulkAction = async (data) => {
    const { action, productIds } = data;

    try {
        let product;

        switch (action) {
        case 'delete':
            product = await Product.updateMany(
                { _id: { $in: productIds } },
                { deletedAt: Date.now() }
            );
            break;
        case 'active':
            product = await Product.updateMany(
                { _id: { $in: productIds } },
                { status: 'active' }
            );
            break;
        case 'inactive':
            product = await Product.updateMany(
                { _id: { $in: productIds } },
                { status: 'inactive' }
            );
            break;
        case 'restore':
            product = await Product.updateMany(
                { _id: { $in: productIds } },
                { deletedAt: null }
            );
            break;
        default:
            throw new HttpError('Invalid action', 400);
        }

        return product;
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

// Get products by category slug
const getByProductSlug = async (slug) => {
    // Find a product by its category slug
    try {
        const products = await Product.aggregate([
            {
                $lookup: {
                    from: 'categories',
                    localField: 'categoryId',
                    foreignField: '_id',
                    as: 'category'
                }
            },
            {
                $unwind: '$category' // Unwind the 'category' array
            },
            {
                $lookup: {
                    from: 'subCategories',
                    localField: 'subCategoryId',
                    foreignField: '_id',
                    as: 'subCategory'
                }
            },
            {
                $unwind: {
                    path: '$subCategory',
                    preserveNullAndEmptyArrays: true // Include documents without matching subCategory
                }
            },
            {
                $lookup: {
                    from: 'brands',
                    localField: 'brandId',
                    foreignField: '_id',
                    as: 'brand'
                }
            },
            {
                $unwind: {
                    path: '$brand',
                    preserveNullAndEmptyArrays: true // Include documents without matching subCategory
                }
            },
            {
                $match: {
                    slug
                }
            }
        ]);

        return products[0];
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

// Get random 10 products
const getFeaturedProducts = async () => {
    try {
        const products = await Product.find({}).limit(10).exec();
        return products;
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Retrieves products by category slug.
 *
 * @param {string} slug - The category slug.
 * @returns {Promise<Array>} - A promise that resolves to an array of products.
 * @throws {HttpError} - If there is an error while retrieving the products.
 */
const getProductsByCategorySlug = async (slug) => {
    try {
        const products = await Product.aggregate([
            {
                $lookup: {
                    from: 'categories',
                    localField: 'categoryId',
                    foreignField: '_id',
                    as: 'category'
                }
            },
            {
                $unwind: '$category' // Unwind the 'category' array
            },
            {
                $lookup: {
                    from: 'subCategories',
                    localField: 'subCategoryId',
                    foreignField: '_id',
                    as: 'subCategory'
                }
            },
            {
                $unwind: {
                    path: '$subCategory',
                    preserveNullAndEmptyArrays: true // Include documents without matching subCategory
                }
            },
            {
                $lookup: {
                    from: 'brands',
                    localField: 'brandId',
                    foreignField: '_id',
                    as: 'brand'
                }
            },
            {
                $unwind: {
                    path: '$brand',
                    preserveNullAndEmptyArrays: true // Include documents without matching subCategory
                }
            },
            {
                $match: {
                    'category.slug': slug
                }
            }
        ]);

        return products;
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

/**
 * Retrieves products along with their category, subcategory, and brand information.
 * @returns {Promise<Array>} An array of products with their associated category, subcategory, and brand information.
 * @throws {HttpError} If there is an error while retrieving the products.
 */
const getProductsAndItsCategory = async () => {
    try {
        const products = await Product.aggregate([
            {
                $lookup: {
                    from: 'categories',
                    localField: 'categoryId',
                    foreignField: '_id',
                    as: 'category'
                }
            },
            {
                $unwind: '$category' // Unwind the 'category' array
            },
            {
                $lookup: {
                    from: 'subCategories',
                    localField: 'subCategoryId',
                    foreignField: '_id',
                    as: 'subCategory'
                }
            },
            {
                $unwind: {
                    path: '$subCategory',
                    preserveNullAndEmptyArrays: true // Include documents without matching subCategory
                }
            },
            {
                $lookup: {
                    from: 'brands',
                    localField: 'brandId',
                    foreignField: '_id',
                    as: 'brand'
                }
            },
            {
                $unwind: {
                    path: '$brand',
                    preserveNullAndEmptyArrays: true // Include documents without matching subCategory
                }
            }
        ]);

        return products;
    } catch (err) {
        throw new HttpError(err.message, 500);
    }
};

export default { get, store, find, update, destroy, bulkAction, count, getByProductSlug, getFeaturedProducts, getProductsByCategorySlug, getProductsAndItsCategory };
