import nodemailer from 'nodemailer';
import 'dotenv/config';

const config = {
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    auth: {
        user: process.env.MAIL_USERNAME,
        pass: process.env.MAIL_PASSWORD
    }
};

const sendMail = async (from, to, subject, message) => {
    const transporter = nodemailer.createTransport(config);

    // Verify connection configuration
    transporter.verify(async function (error, success) {
        if (error) {
            console.log(error);
        } else {
            console.log('Server is ready to take our messages');

            // send mail with defined transport object
            const info = await transporter.sendMail({
                from, // sender address
                to, // list of receivers
                subject, // Subject line
                text: message, // plain text body
                html: message // html body
            }).catch(err => {
                console.log(err);
            });

            console.log('Message sent: %s', info.messageId);
        }
    });
};

export default sendMail;
