import multer from 'multer';
import path from 'path';
import fs from 'fs';
import { UPLOAD_DIR } from '../utils/constants.js';

/**
 * Middleware function to handle file uploads using multer.
 * @param {string} uploadFolder - The folder where the uploaded files will be stored.
 * @returns {function} - Multer middleware function.
 */
export default function imageUploader (uploadFolder, fileFieldName = 'image') {
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            const destination = path.resolve(`${UPLOAD_DIR}/${uploadFolder}`);

            if (!fs.existsSync(destination)) {
                fs.mkdirSync(destination, { recursive: true });
            }

            cb(null, destination);
        },
        filename: function (req, file, cb) {
            const extension = file.mimetype.split('/')[1];
            const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
            const fileName = file.fieldname + '-' + uniqueSuffix + '.' + extension;

            // Attach the generated filename to the request object
            req.generatedFileName = fileName;

            cb(null, fileName);
        }
    });

    const limits = {
        files: 1, // Set the maximum number of files to 1
        fileSize: 5 * 1024 * 1024 // Set the maximum file size to 5MB
    };

    return multer({ storage, limits }).single(fileFieldName);
}
