import express from 'express';
import 'dotenv/config';
import mongoose from 'mongoose';
import logger from './services/loggerService.js';
import './cron-jobs/index.js';
import routes from './routes/index.js';
const app = express();

const PORT = process.env.PORT || process.env.APP_PORT;
app.use(routes);

mongoose.connect(`${process.env.MONGO_URL}${process.env.MONGO_DBNAME}${process.env.MONGO_QUERY_PARAMS}`)
    .then(() => {
        app.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`);
            logger.info(`Server is running on port ${PORT}`);
        });
    })
    .catch(err => {
        console.log(err);
    });
