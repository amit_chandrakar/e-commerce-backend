import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the schema for a category in the e-commerce backend.
 *
 * @typedef {Object} CategorySchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company associated with the category.
 * @property {string} name - The name of the category.
 * @property {string} slug - The slug of the category.
 * @property {string} status - The status of the category. Can be 'active' or 'inactive'.
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created the category.
 * @property {Date} createdAt - The date and time when the category was created.
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the category.
 * @property {Date} updatedAt - The date and time when the category was last updated.
 * @property {Schema.Types.ObjectId} deletedBy - The ID of the user who soft deleted the category.
 * @property {Date} deletedAt - The date and time when the category was soft deleted.
 */
const categorySchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: true },
    name: { type: String, required: true, unique: false },
    slug: { type: String, required: true, unique: true },
    status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false },
    deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
    deletedAt: { type: Date, required: false }
});

categorySchema.plugin(uniqueValidator);

export default categorySchema;
