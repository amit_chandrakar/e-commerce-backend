import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the schema for a shipping address.
 *
 * @typedef {Object} ShippingAddressSchema
 * @property {Schema.Types.ObjectId} userId - The ID of the user associated with the shipping address.
 * @property {string} name - The name associated with the shipping address.
 * @property {string} email - The email associated with the shipping address.
 * @property {string} phone - The phone number associated with the shipping address.
 * @property {string} address - The address associated with the shipping address.
 * @property {string} landmark - The landmark associated with the shipping address.
 * @property {string} houseNumber - The house number associated with the shipping address.
 * @property {string} city - The city associated with the shipping address.
 * @property {string} state - The state associated with the shipping address.
 * @property {string} zip - The zip code associated with the shipping address.
 * @property {string} country - The country associated with the shipping address.
 * @property {boolean} isDefaultAddress - Indicates if the shipping address is the default address.
 * @property {string} status - The status of the shipping address. Possible values are 'active' and 'inactive'.
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created the shipping address.
 * @property {Date} createdAt - The date and time when the shipping address was created.
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the shipping address.
 * @property {Date} updatedAt - The date and time when the shipping address was last updated.
 * @property {Schema.Types.ObjectId} deletedBy - The ID of the user who deleted the shipping address (soft delete).
 * @property {Date} deletedAt - The date and time when the shipping address was deleted (soft delete).
 */
const shippingAddressSchema = new Schema({
    userId: { type: Schema.Types.ObjectId, required: true },
    name: { type: String, required: true },
    email: { type: String, required: false },
    phone: { type: String, required: true },
    address: { type: String, required: true },
    landmark: { type: String, required: true },
    houseNumber: { type: String, required: true },
    city: { type: String, required: true },
    state: { type: String, required: true },
    zip: { type: String, required: true },
    country: { type: String, required: true },
    isDefaultAddress: { type: Boolean, default: false }, // Default address
    status: { type: String, enum: ['active', 'inactive'], default: 'active' },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false },
    deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
    deletedAt: { type: Date, required: false }
}, { collection: 'shippingAddresses' });

shippingAddressSchema.plugin(uniqueValidator);

export default shippingAddressSchema;
