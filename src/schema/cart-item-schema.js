import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the schema for a cart item.
 * @typedef {Object} CartItemSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company.
 * @property {Schema.Types.ObjectId} cartId - The ID of the cart.
 * @property {Schema.Types.ObjectId} productId - The ID of the product.
 * @property {number} unitPrice - The unit price of the item.
 * @property {number} quantity - The quantity of the item.
 * @property {number} totalPrice - The total price of the item.
 * @property {Schema.Types.ObjectId} [deletedBy] - The ID of the user who deleted the item (optional).
 * @property {Date} [deletedAt] - The date and time when the item was deleted (optional).
 */
const cartItemSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: false },
    cartId: { type: Schema.Types.ObjectId, required: true },
    productId: { type: Schema.Types.ObjectId, required: true },
    unitPrice: { type: Number, required: true },
    quantity: { type: Number, required: true },
    totalPrice: { type: Number, required: true },
    deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
    deletedAt: { type: Date, required: false }
}, { collection: 'cartItems' });

cartItemSchema.plugin(uniqueValidator);

export default cartItemSchema;
