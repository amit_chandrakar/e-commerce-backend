import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the schema for a brand in the e-commerce backend.
 * @typedef {Object} BrandSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company associated with the brand.
 * @property {string} name - The name of the brand.
 * @property {string} slug - The slug of the brand, used for URL generation.
 * @property {string} status - The status of the brand, can be 'active' or 'inactive'.
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created the brand.
 * @property {Date} createdAt - The date and time when the brand was created.
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the brand.
 * @property {Date} updatedAt - The date and time when the brand was last updated.
 * @property {Schema.Types.ObjectId} deletedBy - The ID of the user who soft deleted the brand.
 * @property {Date} deletedAt - The date and time when the brand was soft deleted.
 */
const brandSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: true },
    name: { type: String, required: true, unique: false },
    slug: { type: String, required: true, unique: true },
    status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false },
    deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
    deletedAt: { type: Date, required: false }
});

brandSchema.plugin(uniqueValidator);

export default brandSchema;
