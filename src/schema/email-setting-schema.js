import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the schema for email settings.
 * @typedef {Object} EmailSettingSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company.
 * @property {string} mailFromName - The name of the sender.
 * @property {string} mailFromAddress - The email address of the sender.
 * @property {string} driver - The email driver.
 * @property {string} host - The email host.
 * @property {number} port - The email port.
 * @property {string} username - The username for email authentication.
 * @property {string} password - The password for email authentication.
 * @property {string} encryption - The encryption method for email.
 * @property {string} status - The status of the email setting. Can be 'active' or 'inactive'.
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created the email setting.
 * @property {Date} createdAt - The date and time when the email setting was created.
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the email setting.
 * @property {Date} updatedAt - The date and time when the email setting was last updated.
 */
const emailSettingSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: true },
    mailFromName: { type: String, required: true },
    mailFromAddress: { type: String, required: true },
    driver: { type: String, required: true },
    host: { type: String, required: true },
    port: { type: Number, required: true },
    username: { type: String, required: true },
    password: { type: String, required: true },
    encryption: { type: String, required: true },
    status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false }
}, { collection: 'emailSettings' });

emailSettingSchema.plugin(uniqueValidator);

export default emailSettingSchema;
