import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the tax setting schema.
 *
 * @typedef {Object} TaxSettingSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company.
 * @property {string} name - The name of the tax setting.
 * @property {string} rate - The tax rate.
 * @property {string} status - The status of the tax setting. Can be 'active' or 'inactive'.
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created the tax setting.
 * @property {Date} createdAt - The date and time when the tax setting was created.
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the tax setting.
 * @property {Date} updatedAt - The date and time when the tax setting was last updated.
 */
const taxSettingSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: true },
    name: { type: String, required: true },
    rate: { type: String, required: true },
    status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false },
    deletedBy: { type: Schema.Types.ObjectId, required: false },
    deletedAt: { type: Date, default: null }
});

taxSettingSchema.plugin(uniqueValidator);

export default taxSettingSchema;
