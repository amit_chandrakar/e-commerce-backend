import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * @typedef {Object} SubCategorySchema
 * @property {Schema.Types.ObjectId} categoryId - The ID of the category to which the subcategory belongs.
 * @property {string} slug - The unique slug for the subcategory.
 * @property {string} name - The name of the subcategory.
 * @property {string} status - The status of the subcategory. Can be 'active' or 'inactive'. Defaults to 'inactive'.
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created the subcategory.
 * @property {Date} createdAt - The date and time when the subcategory was created. Defaults to the current date and time.
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the subcategory.
 * @property {Date} updatedAt - The date and time when the subcategory was last updated.
 * @property {Schema.Types.ObjectId} deletedBy - The ID of the user who deleted the subcategory (soft delete).
 * @property {Date} deletedAt - The date and time when the subcategory was deleted (soft delete).
 */
const subCategorySchema = new Schema({
    categoryId: { type: Schema.Types.ObjectId, required: true },
    slug: { type: String, required: true, unique: true },
    name: { type: String, required: true, unique: true },
    status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false },
    deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
    deletedAt: { type: Date, required: false }
}, { collection: 'subCategories' });

subCategorySchema.plugin(uniqueValidator);

export default subCategorySchema;
