import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Payment Schema
 * @typedef {Object} paymentSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company associated with the payment
 * @property {Schema.Types.ObjectId} orderId - The ID of the order associated with the payment
 * @property {Schema.Types.ObjectId} paymentId - The ID of the payment method
 * @property {String} status - The status of the payment (Incompleted, Completed, Processing, Failed)
 * @property {Number} amount - The amount of the payment
 * @property {Number} paymentGatewayResponse - The response from the payment gateway
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created the payment
 * @property {Date} createdAt - The date and time when the payment was created
 */
const paymentSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: false },
    orderId: { type: Schema.Types.ObjectId, required: false },
    paymentMethod: { type: String, required: false }, // Payment method
    status: { type: String, enum: [null, 'Incompleted', 'Completed', 'Processing', 'Failed'], default: null },
    amount: { type: Number, default: 0 },
    paymentId: { type: Schema.Types.ObjectId, required: false },
    paymentGatewayResponse: { type: Number, default: null },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now }
});

paymentSchema.plugin(uniqueValidator);

export default paymentSchema;
