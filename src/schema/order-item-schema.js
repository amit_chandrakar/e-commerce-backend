import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the schema for an order item.
 * @typedef {Object} OrderItemSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company associated with the order item.
 * @property {Schema.Types.ObjectId} orderId - The ID of the order associated with the order item.
 * @property {Schema.Types.ObjectId} productId - The ID of the product associated with the order item.
 * @property {number} unitPrice - The unit price of the order item.
 * @property {number} quantity - The quantity of the order item.
 * @property {number} totalPrice - The total price of the order item.
 * @property {Schema.Types.ObjectId} [deletedBy] - The ID of the user who deleted the order item (optional).
 * @property {Date} [deletedAt] - The date and time when the order item was deleted (optional).
 */
const orderItemSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: true },
    orderId: { type: Schema.Types.ObjectId, required: true },
    productId: { type: Schema.Types.ObjectId, required: true },
    unitPrice: { type: Number, required: true },
    quantity: { type: Number, required: true },
    totalPrice: { type: Number, required: true },
    deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
    deletedAt: { type: Date, required: false }
}, { collection: 'orderItems' });

orderItemSchema.plugin(uniqueValidator);

export default orderItemSchema;
