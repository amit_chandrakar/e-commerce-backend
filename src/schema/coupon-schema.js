import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the schema for a coupon.
 *
 * @typedef {Object} CouponSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company associated with the coupon.
 * @property {string} code - The code of the coupon.
 * @property {string} discountType - The type of discount for the coupon. Can be 'percentage' or 'fixed'.
 * @property {number} discount - The amount of discount for the coupon.
 * @property {Date} fromDate - The start date of the coupon validity period.
 * @property {Date} toDate - The end date of the coupon validity period.
 * @property {string} status - The status of the coupon. Can be 'active' or 'inactive'.
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created the coupon.
 * @property {Date} createdAt - The date and time when the coupon was created.
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the coupon.
 * @property {Date} updatedAt - The date and time when the coupon was last updated.
 * @property {Schema.Types.ObjectId} deletedBy - The ID of the user who deleted the coupon (soft delete).
 * @property {Date} deletedAt - The date and time when the coupon was deleted (soft delete).
 */
const couponSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: true },
    code: { type: String, required: true },
    discountType: { type: String, enum: ['percentage', 'flat'], required: true },
    discount: { type: Number, required: true },
    fromDate: { type: Date, required: false },
    toDate: { type: Date, required: false },
    status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false },
    deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
    deletedAt: { type: Date, required: false }
});

couponSchema.plugin(uniqueValidator);

export default couponSchema;
