import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the schema for an order in the e-commerce system.
 *
 * @typedef {Object} OrderSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company associated with the order.
 * @property {Schema.Types.ObjectId} userId - The ID of the user who placed the order.
 * @property {Schema.Types.ObjectId} couponId - The ID of the coupon applied to the order (optional).
 * @property {Schema.Types.ObjectId} paymentId - The ID of the payment method used for the order.
 * @property {Schema.Types.ObjectId} addressId - The ID of the delivery address for the order.
 * @property {number} subTotal - The subtotal amount of the order.
 * @property {number} discount - The discount amount applied to the order (default: 0).
 * @property {number} tax - The tax amount applied to the order (default: 0).
 * @property {number} deliveryFee - The delivery fee amount for the order (default: 0).
 * @property {number} total - The total amount of the order.
 * @property {string} deliveryInstruction - The delivery instruction for the order (optional).
 * @property {string} status - The status of the order. Can be one of 'Ordered', 'Canceled', or 'Delivered' (default: 'Ordered').
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created the order (optional).
 * @property {Date} createdAt - The date and time when the order was created (default: current date and time).
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the order (optional).
 * @property {Date} updatedAt - The date and time when the order was last updated (optional).
 * @property {Schema.Types.ObjectId} deletedBy - The ID of the user who deleted the order (optional). (Soft delete)
 * @property {Date} deletedAt - The date and time when the order was deleted (optional).
 */
const orderSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: false },
    userId: { type: Schema.Types.ObjectId, required: false },
    couponId: { type: Schema.Types.ObjectId, required: false },
    paymentId: { type: Schema.Types.ObjectId, required: false },
    shippingAddressId: { type: Schema.Types.ObjectId, required: false },
    orderNumber: { type: String, required: false },
    subTotal: { type: Number, required: false },
    discount: { type: Number, default: 0 },
    tax: { type: Number, default: 0 },
    deliveryFee: { type: Number, default: 0 },
    total: { type: Number, required: false },
    deliveryInstruction: { type: String, required: false },
    status: { type: String, enum: ['Ordered', 'Canceled', 'Delivered'], default: 'Ordered' },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false },
    deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
    deletedAt: { type: Date, required: false }
});

orderSchema.plugin(uniqueValidator);

export default orderSchema;
