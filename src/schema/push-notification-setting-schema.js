import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Mongoose schema for push notification settings.
 *
 * @typedef {Object} PushNotificationSettingSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company associated with the settings.
 * @property {string} onesignalAppId - The OneSignal App ID for push notifications.
 * @property {string} onesignalApiKey - The OneSignal API key for push notifications.
 * @property {string} notificationLogo - The URL of the notification logo (optional).
 * @property {string} status - The status of the push notification settings. Possible values: 'active', 'inactive'. Default: 'inactive'.
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the settings (optional).
 * @property {Date} updatedAt - The date when the settings were last updated (optional).
 *
 * @memberof module:push-notification-setting-schema
 */
const pushNotificationSettingSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: true },
    onesignalAppId: { type: String, required: true },
    onesignalApiKey: { type: String, required: true },
    notificationLogo: { type: String, required: false },
    status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false }
}, { collection: 'pushNotificationSettings' });

pushNotificationSettingSchema.plugin(uniqueValidator);

export default pushNotificationSettingSchema;
