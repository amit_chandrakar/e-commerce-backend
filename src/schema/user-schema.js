import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * User Schema
 * @typedef {Object} UserSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company the user belongs to (optional)
 * @property {string} name - The name of the user (required)
 * @property {string} email - The email of the user (required, unique)
 * @property {string} password - The password of the user (required, minimum length: 6)
 * @property {string} mobile - The mobile number of the user (required, minimum length: 10, unique)
 * @property {string} dob - The date of birth of the user (optional)
 * @property {string} gender - The gender of the user (enum: ['male', 'female', 'other', null], default: null)
 * @property {string} image - The image URL of the user (optional)
 * @property {string} role - The role of the user (enum: ['customer', 'admin', 'employee', 'superadmin'], default: 'customer')
 * @property {string} status - The status of the user (enum: ['active', 'inactive'], default: 'inactive')
 * @property {Date} verifiedAt - The date when the user was verified (optional)
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created this user (optional)
 * @property {Date} createdAt - The date when the user was created (default: current date)
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated this user (optional)
 * @property {Date} updatedAt - The date when the user was last updated (optional)
 * @property {Schema.Types.ObjectId} deletedBy - The ID of the user who soft deleted this user (optional)
 * @property {Date} deletedAt - The date when the user was soft deleted (optional)
 */
const userSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: false },
    employeeId: { type: String, required: false },
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true, minlength: 6 },
    mobile: { type: String, required: true, minlength: 10, unique: true },
    dob: { type: String, required: false },
    gender: { type: String, enum: ['male', 'female', 'other', null], default: null },
    image: { type: String, required: false },
    role: { type: String, enum: ['customer', 'admin', 'employee', 'superadmin'], default: 'customer' },
    status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    verifiedAt: { type: Date, required: false },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false },
    deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
    deletedAt: { type: Date, required: false },
    lastLogin: { type: Date, required: false }
});

userSchema.plugin(uniqueValidator);

export default userSchema;
