import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Mongoose schema for social authentication settings.
 *
 * @typedef {Object} SocialAuthSettingSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company associated with the settings (required).
 * @property {string} facebookStatus - The status of Facebook authentication ('active' or 'inactive') (default: 'inactive').
 * @property {string} facebookClientId - The client ID for Facebook authentication (optional).
 * @property {string} facebookClientSecret - The client secret for Facebook authentication (optional).
 * @property {string} googleClientId - The client ID for Google authentication (optional).
 * @property {string} googleClientSecret - The client secret for Google authentication (optional).
 * @property {string} googleStatus - The status of Google authentication ('active' or 'inactive') (default: 'inactive').
 * @property {string} twitterClientId - The client ID for Twitter authentication (optional).
 * @property {string} twitterSecretId - The secret ID for Twitter authentication (optional).
 * @property {string} twitterStatus - The status of Twitter authentication ('active' or 'inactive') (default: 'inactive').
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the settings (optional).
 * @property {Date} updatedAt - The date when the settings were last updated (optional).
 *
 * @memberof module:src/schema/social-auth-setting-schema
 */
const socialAuthSettingSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: true },
    facebookStatus: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    facebookClientId: { type: String, required: false },
    facebookClientSecret: { type: String, required: false },
    googleClientId: { type: String, required: false },
    googleClientSecret: { type: String, required: false },
    googleStatus: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    twitterClientId: { type: String, required: false },
    twitterSecretId: { type: String, required: false },
    twitterStatus: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false }
}, { collection: 'socialAuthSettings' });

socialAuthSettingSchema.plugin(uniqueValidator);

export default socialAuthSettingSchema;
