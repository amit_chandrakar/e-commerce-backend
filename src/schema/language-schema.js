import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
const Schema = mongoose.Schema;

/**
 * Represents the language setting schema.
 *
 * @typedef {Object} LanguageSettingSchema
 * @property {Schema.Types.ObjectId} companyId - The ID of the company.
 * @property {string} name - The name of the language.
 * @property {string} code - The code of the language.
 * @property {string} status - The status of the language (active or inactive).
 * @property {Schema.Types.ObjectId} createdBy - The ID of the user who created the language.
 * @property {Date} createdAt - The date when the language was created.
 * @property {Schema.Types.ObjectId} updatedBy - The ID of the user who last updated the language.
 * @property {Date} updatedAt - The date when the language was last updated.
 */
const languageSettingSchema = new Schema({
    companyId: { type: Schema.Types.ObjectId, required: true },
    name: { type: String, required: true },
    code: { type: String, required: true },
    status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false },
    deletedBy: { type: Schema.Types.ObjectId, required: false },
    deletedAt: { type: Date, default: null }
});

languageSettingSchema.plugin(uniqueValidator);

export default languageSettingSchema;
