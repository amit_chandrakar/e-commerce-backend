import mongoose from 'mongoose';
const Schema = mongoose.Schema;

/**
 * Represents the schema for a company.
 * @typedef {Object} CompanySchema
 * @property {string} name - The name of the company.
 * @property {string} email - The email of the company.
 * @property {string} phone - The phone number of the company.
 * @property {string} website - The website of the company.
 * @property {string} address - The address of the company.
 * @property {string} dateFormat - The date format used by the company.
 * @property {string} timeFormat - The time format used by the company.
 * @property {string} timeZone - The timeZone of the company.
 * @property {string} language - The language used by the company.
 * @property {string} appName - The name of the company's application.
 * @property {string} logo - The logo of the company.
 * @property {string} favicon - The favicon of the company.
 * @property {string} miniDrawer - The mini drawer of the company.
 */
const companySchema = new Schema({
    name: { type: String, required: false },
    email: { type: String, required: false },
    phone: { type: String, required: false },
    website: { type: String, required: false },
    address: { type: String, required: false },
    dateFormat: { type: String, required: false },
    timeFormat: { type: String, required: false },
    timeZone: { type: String, required: false },
    language: { type: String, required: false },
    appName: { type: String, required: false },
    logo: { type: String, required: false },
    favicon: { type: String, required: false },
    miniDrawer: { type: String, required: false },
    status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
    createdBy: { type: Schema.Types.ObjectId, required: false },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: Schema.Types.ObjectId, required: false },
    updatedAt: { type: Date, required: false },
    deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
    deletedAt: { type: Date, required: false }

});

export default companySchema;
