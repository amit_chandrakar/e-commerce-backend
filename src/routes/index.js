import express from 'express';
import cors from 'cors';
import backEndRoutes from './back-end/index.js';
import frontEndRoutes from './front-end/index.js';
import { FRONTEND_API_ENDPOINT, BACKEND_API_ENDPOINT } from '../config/app.js';
import bodyParser from 'body-parser';
import { UPLOAD_DIR } from '../utils/constants.js';
import HttpError from '../services/httpErrorService.js';
import fs from 'fs';
const app = express();

app.use(bodyParser.json());
app.use(cors());

// Image Upload to local storage
app.use('/public', express.static('public'));
app.use(express.static(UPLOAD_DIR));

// Backend routes
app.use(FRONTEND_API_ENDPOINT, frontEndRoutes);

// Backend routes
app.use(BACKEND_API_ENDPOINT, backEndRoutes);

app.use((req, res, next) => {
    const error = new HttpError('Could not find this route.', 404);
    throw error;
});

app.use((error, req, res, next) => {
    if (req.file) {
        fs.unlink(req.file.path, err => {
            console.log(err);
        });
    }
    if (res.headerSent) {
        return next(error);
    }

    res.status(error.code || 500);

    const response = {
        status: 'error',
        message: error.message || 'An unknown error occurred!'
    };

    res.json(response);
});

export default app;
