import express from 'express';
import orderController from '../../controllers/back-end/order-controller.js';

const router = express.Router();

router.get('/', orderController.get);
router.get('/edit/:orderId', orderController.edit);
router.put('/update/:orderId', orderController.update);
router.get('/total-categories', orderController.count);
router.post('/bulk-action', orderController.bulkAction);

export default router;
