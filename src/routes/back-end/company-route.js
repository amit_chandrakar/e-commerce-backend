import express from 'express';
import companyController from '../../controllers/back-end/company-controller.js';

const router = express.Router();

router.post('/store', companyController.store);
router.get('/show', companyController.show);
router.put('/update', companyController.logo, companyController.update);

export default router;
