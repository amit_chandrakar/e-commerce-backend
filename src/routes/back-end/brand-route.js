import express from 'express';
import brandController from '../../controllers/back-end/brand-controller.js';

const router = express.Router();

router.get('/', brandController.get);
router.post('/store', brandController.store);
router.get('/edit/:brandId', brandController.edit);
router.get('/show/:slug', brandController.findBySlug);
router.put('/update/:brandId', brandController.update);
router.delete('/delete/:brandId', brandController.destroy);
router.post('/bulk-action', brandController.bulkAction);

export default router;
