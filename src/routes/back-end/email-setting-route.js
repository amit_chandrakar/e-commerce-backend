import express from 'express';
import emailSettingController from '../../controllers/back-end/email-setting-controller.js';

const router = express.Router();

router.get('/show', emailSettingController.show);
router.put('/update', emailSettingController.update);

export default router;
