import express from 'express';
import categoryController from '../../controllers/back-end/category-controller.js';

const router = express.Router();

router.get('/', categoryController.get);
router.post('/store', categoryController.store);
router.get('/edit/:categoryId', categoryController.edit);
router.put('/update/:categoryId', categoryController.update);
router.delete('/delete/:categoryId', categoryController.destroy);
router.get('/total-categories', categoryController.count);
router.post('/bulk-action', categoryController.bulkAction);

export default router;
