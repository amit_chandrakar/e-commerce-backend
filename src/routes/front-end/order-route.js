import express from 'express';
import orderController from '../../controllers/back-end/order-controller.js';
const router = express.Router();
// Other routes...
router.post('/store/:sessionId', orderController.store);

export default router;
